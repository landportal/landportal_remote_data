Clear indexes

```
drush search-api-clear remote_libraries && drush search-api-clear remote_events && drush search-api-clear remote_blogs && drush search-api-clear remote_organizations && drush search-api-clear remote_projects && drush search-api-clear remote_news
```

Reindex

```
drush search-api-index remote_events && drush search-api-index remote_blogs && drush search-api-index remote_organizations && drush search-api-index remote_projects && drush search-api-index remote_news && drush search-api-index remote_libraries
```